
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ page import = "com.codesplai.spring.demo.models.User" %> 
<%@ page import = "java.util.*" %> 

<%@ page import = "org.springframework.validation.*" %> 

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@include file="parts/head.html" %>
<%@include file="parts/nav.html" %>

<div class="container">

  <div class="row">
    <div class="col">
    <br>
    <br>
        <h1>Editar usuari</h1>
    <br>
    <form:form action="${pageContext.request.contextPath}/user/update/${user.id}" method="post" modelAttribute="user">
      <div class="col-md-4 mb-3">
        <spring:message code="lbl.name" text="Nom" />
        <form:input path="name" class="form-control" />
        <form:errors path="name" cssClass="error" />
      </div>
      <div class="col-md-4 mb-3">
        <spring:message code="lbl.email" text="Email" />
        <form:input path="email" class="form-control" />
        <form:errors path="email" cssClass="error" />
      </div>
      <input type="submit" value="enviar usr"/> 
    </form:form>
    <div>
  <div>
<div>

<%@include file="parts/foot.html" %>




