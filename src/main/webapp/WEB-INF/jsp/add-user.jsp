<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ page import = "com.codesplai.spring.demo.models.User" %> 
<%@ page import = "java.util.*" %> 

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
 

<%@include file="parts/head.html" %>
<%@include file="parts/nav.html" %>

<div class="container">

  <div class="row">
    <div class="col">
    <br>
    <br>
        <h1>Afegir usuari</h1>
    <br>

      <form:form action="${pageContext.request.contextPath}/user/adduser" method="post" modelAttribute="user">
        <spring:message code="lbl.name" text="Nom" />
        <form:input path="name" class="form-control" />
        <form:errors path="name" cssClass="error" />
        <br>
        <spring:message code="lbl.email" text="Email" />
        <form:input path="email" class="form-control" />
        <form:errors path="email" cssClass="error" />
        <br>
        <button class="btn btn-success" type="submit" value="">Desar</button> 
      </form:form>
    <div>
  <div>
<div>

<%@include file="parts/foot.html" %>




