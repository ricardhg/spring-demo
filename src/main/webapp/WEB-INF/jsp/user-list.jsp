<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ page import = "com.codesplai.spring.demo.models.User" %> 
<%@ page import = "java.util.*" %> 

<%

List<User> usuaris = (List) request.getAttribute("users"); 

 %>

<%@include file="parts/head.html" %>
<%@include file="parts/nav.html" %>

<div class="container">
<br>
<br>
 <h1>Usuaris</h1>
<br>

    <table class="table">
    <thead>
    <tr>
    <th>#</th>
    <th>Nom</th>
    <th>Email</th>
    <th></th>
    <th></th>
    </tr>
    </thead>
    <tbody>

    <% for(User u : usuaris) { %>
        <tr>
        <td><%= u.getId() %></td>
        <td><%= u.getName() %></td>
        <td><%= u.getEmail() %></td>
        <td><a href="/user/edit/<%= u.getId() %>">Edit</a></td>
        <td><a href="/user/delete/<%= u.getId() %>">Delete</a></td>
        </tr>
    <% } %>
    </tbody>
    </table>
    

<div>

<%@include file="parts/foot.html" %>


