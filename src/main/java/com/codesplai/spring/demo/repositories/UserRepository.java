package com.codesplai.spring.demo.repositories;

import com.codesplai.spring.demo.models.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {}

